<--! a. artist that has letter D in their name -->
SELECT * FROM artists WHERE name LIKE "%d%";
<--! b. songs that has a length less than 230 -->
SELECT * FROM songs WHERE length < 230;
<--! c. join albums and songs -->
SELECT album_title, song_name, length 
FROM albums JOIN songs ON albums.id = songs.album_id;
<--! d. join artists and albums with letter 'a' in album -->
SELECT * FROM albums JOIN artists ON albums.artist_id = artists.id
WHERE album_title LIKE "%a%";
<--! e. Sort albums in Z-A and only show 4 -->
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
<--! f. join albums and songs and sort albums in Z-A and songs in A-Z-->
SELECT *  
FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY album_title DESC, song_name;